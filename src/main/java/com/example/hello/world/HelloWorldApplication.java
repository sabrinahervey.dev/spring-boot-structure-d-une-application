package com.example.hello.world;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloWorldApplication
implements CommandLineRunner
 {
  public static void main(String[] args) {
	  SpringApplication.run(HelloWorldApplication.class, args);
  }

  @Override
  public void run(String... args) {
      System.out.println("hello World");
  }
}
